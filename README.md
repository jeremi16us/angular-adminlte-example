# Angular AdminLTE Application Creation a Initiation
1. $ ng new angular-adminlte
2. $ npm i admin-lte@^3.0 --save
3. $ npm install
4. $ ng serve --open --port 1410
5. $ ng generate component demo
6. open AdminLTE Source Code => view-source:https://adminlte.io/themes/dev/AdminLTE/index2.html
7. edit angular.json and insert below code
```
"styles": [
  "src/styles.css",
  "node_modules/admin-lte/plugins/fontawesome-free/css/all.min.css",
  "node_modules/admin-lte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css",
  "node_modules/admin-lte/docs_html/assets/css/adminlte.css"
],
"scripts": [
  "node_modules/admin-lte/plugins/jquery/jquery.js",
  "node_modules/admin-lte/plugins/bootstrap/js/bootstrap.bundle.js",
  "node_modules/admin-lte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.js",
  "node_modules/admin-lte/dist/js/adminlte.js",
  "node_modules/admin-lte/dist/js/demo.js",
  "node_modules/admin-lte/plugins/jquery-mousewheel/jquery.mousewheel.js",
  "node_modules/admin-lte/plugins/raphael/raphael.js",
  "node_modules/admin-lte/plugins/jquery-mapael/jquery.mapael.js",
  "node_modules/admin-lte/plugins/jquery-mapael/maps/usa_states.js",
  "node_modules/admin-lte/plugins/chart.js/Chart.js",
  "node_modules/admin-lte/dist/js/pages/dashboard2.js"
]
```
8. Generating services
8.1 ng g s services/auth
9. Generating Guard
9.1 ng g guard guard/auth
10. Generating Module
10.1 ng g module view/portal --routing
10.2 ng g c view/portal -is
10.3 ng g c view/portal/user-list -is
10.4 ng g class model/user --type=model
10.5 ng add @angular/material
10.6 ng add @ng-bootstrap/ng-bootstrap

# Configure PM2 Process
1. $ pm2 start "ng serve --host 0.0.0.0 --port 1410 --disableHostCheck true" --name angular-adminlte-example
2. $ pm2 startup
3. $ pm2 save
### Remove Process
1. $ pm2 delete angular-adminlte-example
2. $ pm2 unstartup systemd
3. $ pm2 save --force