import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DemoAdminLteComponent } from './demo-admin-lte/demo-admin-lte.component';
import { DemoAngularComponent } from './demo-angular/demo-angular.component';

import { PageNotFoundComponent } from './view/common/page-not-found/page-not-found.component';
import { FooterComponent } from './view/common/footer/footer.component';
import { HeaderComponent } from './view/common/header/header.component';

import { HomeComponent } from './view/home/home.component';
import { LoginComponent } from './view/login/login.component';

import { AuthGuard } from './guard/auth.guard';

const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    /*{ path: 'portal', component: PortalComponent, canActivate: [ AuthGuard ] },*/
    { path: 'demo-admin-lte', component: DemoAdminLteComponent },
    { path: 'demo-angular', component: DemoAngularComponent },
    {
        path: 'portal',
        loadChildren: () => import('./view/portal/portal.module').then(mod => mod.PortalModule)
    },
    {
        path: 'console',
        loadChildren: () => import('./view/console/console.module').then(mod => mod.ConsoleModule)
    },
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingComponent { }
export const routingComponets = [
    DemoAdminLteComponent,
    DemoAngularComponent,
    HomeComponent,
    LoginComponent,
    FooterComponent,
    HeaderComponent,
    PageNotFoundComponent
]