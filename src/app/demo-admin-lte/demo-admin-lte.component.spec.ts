import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoAdminLteComponent } from './demo-admin-lte.component';

describe('DemoAdminLteComponent', () => {
  let component: DemoAdminLteComponent;
  let fixture: ComponentFixture<DemoAdminLteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoAdminLteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoAdminLteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
