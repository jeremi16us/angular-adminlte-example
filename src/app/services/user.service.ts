import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { User } from 'src/app/model/user.model';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(private http: HttpClient) { }

    saveUser(user) {
        let saveUrl = environment.app.api_url + '/user/update';
        if(user._id){
            saveUrl = environment.app.api_url + '/user/update';
        }

        return this.http.post<User>(saveUrl, user);
    }

    getUser(id) {
       return this.http.get<User>(environment.app.api_url + '/user/get', {
            params: {
                id: id,
            }
        });
    }

    searchUser(query) {
       return this.http.get<User[]>(environment.app.api_url + '/user/list', {
            params: {
                q: query,
            }
        });
    }
}
