import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private serverUrl = "http://localhost:1229/api";
    private loginUrl = this.serverUrl + "/login";

    constructor(private http: HttpClient, private _router: Router) { }

    loginUser(loginUserData){
        return this.http.post<any>(this.loginUrl, loginUserData);
    }

    logoutUser(){
        localStorage.removeItem('currentUser');
        localStorage.removeItem('token');
        this._router.navigate(['/']);
    }

    getCurrentUser(){
        return localStorage.getItem('currentUser');
    }

    getToken(){
        return localStorage.getItem('token');
    }

    isLoggedIn(){
        return !!this.getToken();
    }
}
