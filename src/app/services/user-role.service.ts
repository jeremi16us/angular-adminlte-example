import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { UserRole } from 'src/app/model/user-role.model';

@Injectable({
    providedIn: 'root'
})
export class UserRoleService {

    constructor(private http: HttpClient) { }

    saveUserRole(userRole) {
        let saveUrl = environment.app.api_url + '/userRole/add';
        if(userRole._id){
            saveUrl = environment.app.api_url + '/userRole/update';
        }

        return this.http.post<UserRole>(saveUrl, userRole);
    }

    getUserRole(id) {
        return this.http.get<UserRole>(environment.app.api_url + '/userRole/get', {
            params: {
                id: id,
            }
        });
    }

    searchUserRole(query) {
        return this.http.get<UserRole[]>(environment.app.api_url + '/userRole/list', {
            params: {
                q: query,
            }
        });
    }
}
