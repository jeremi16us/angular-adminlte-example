import { NgModule } from '@angular/core';

import { ConsoleRoutingModule, consoleRoutingComponets } from './console-routing.module';
import { ConsoleComponent } from './console.component';


@NgModule({
    declarations: [
        ConsoleComponent, 
        consoleRoutingComponets
    ],
    imports: [
        ConsoleRoutingModule
    ]
})
export class ConsoleModule { }
