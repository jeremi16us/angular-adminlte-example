import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PortalComponent } from './portal.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UserRoleListComponent } from './user-role-list/user-role-list.component';
import { UserRoleFormComponent } from './user-role-form/user-role-form.component';
import { ConfigurationListComponent } from './configuration-list/configuration-list.component';
import { SystemSettingListComponent } from './system-setting-list/system-setting-list.component';

import { HeaderComponent } from './common/header/header.component';
import { FooterComponent } from './common/footer/footer.component';
import { SideNavComponent } from './common/side-nav/side-nav.component';
import { ContentWrapperComponent } from './common/content-wrapper/content-wrapper.component';

const routes: Routes = [
    { 
        path: '', component: PortalComponent,
        children: [
            { path: '', component: DashboardComponent },
            { path: 'dashboard', component: DashboardComponent },
            { path: 'user-list', component: UserListComponent },
            { path: 'user-form', component: UserFormComponent },
            { path: 'user-role-list', component: UserRoleListComponent },
            { path: 'user-role-form', component: UserRoleFormComponent },
            { path: 'configuration-list', component: ConfigurationListComponent },
            { path: 'system-setting-list', component: SystemSettingListComponent },
        ],
     },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PortalRoutingModule { }
export const portalRoutingComponets = [
    HeaderComponent,
    FooterComponent,
    SideNavComponent,
    ContentWrapperComponent,
    DashboardComponent,
    UserListComponent,
    UserFormComponent,
    UserRoleListComponent,
    UserRoleFormComponent,
    ConfigurationListComponent,
    SystemSettingListComponent,
]