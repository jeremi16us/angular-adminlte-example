import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/model/user.model';
import { UserService } from 'src/app/services/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
    selector: 'app-user-form',
    templateUrl: './user-form.component.html',
    styles: [
    ]
})
export class UserFormComponent implements OnInit {

    currentPageTitle = "User Form";
    id = '';
    user: User;
    userFormGroup: FormGroup;
    isValidForm: boolean;
    prepareFomrprogress = false;

    constructor(private activatedRoute: ActivatedRoute,
        private readonly formBuilder: FormBuilder,
        private readonly snack: MatSnackBar,
        private userService: UserService) { }

    ngOnInit(): void {
        this.prepareFomrprogress = true;
        this.activatedRoute.queryParams.subscribe(params => {
            this.id = params['id'];
            console.log("SELECTED ID : " + this.id);
            if (this.id) {
                this.getUser(this.id)
                    .then(user=>this.init(user))
                    .catch(reason => {
                        console.log(reason);
                        this.init();
                    });
            } else {
                this.init();
            }
        }, error => {
            console.log(error);
            this.init();
        });
    }

    private init(user?: User) {
        this.prepareFomrprogress = false;
        this.userFormGroup = this.formBuilder.group({
            _id: [user?._id, []],
            first_name: [user?.first_name, [Validators.nullValidator, Validators.required]],
            middle_name: [user?.middle_name, [Validators.nullValidator, Validators.required]],
            last_name: [user?.last_name, [Validators.nullValidator, Validators.required]],
            email: [user?.email, [Validators.nullValidator, Validators.required]],
            password: ['', [] ],
            confirm_password: ['', [] ],
        });
    }

    async getUser(userId: string): Promise<User> {
        const user = await this.userService.getUser(userId).toPromise();
        return user;
    }

    saveUser(): void {
        this.isValidForm = this.userFormGroup.valid;
        if (this.isValidForm) {
            this.user = this.userFormGroup.value;
            this.userService.saveUser(this.user).subscribe(value => {
                console.log(value);
            }, err => {
                console.log(err);
            });
        } else {
            this.snack.open('Form is not valid','Ok',{
                duration: 3000
            })
        }
    }
}
