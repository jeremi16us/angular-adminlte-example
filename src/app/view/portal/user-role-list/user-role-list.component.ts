import { Component, OnInit } from '@angular/core';
import { UserRole } from 'src/app/model/user-role.model';
import { UserRoleService } from 'src/app/services/user-role.service';
import { of, Observable } from 'rxjs';

@Component({
    selector: 'app-user-role-list',
    templateUrl: './user-role-list.component.html',
    styles: [
    ]
})
export class UserRoleListComponent implements OnInit {

    public currentPageTitle = "User Role List";
    public listOfUserRole: Observable<UserRole[]>;
    public query = "";

    constructor(private userRoleService: UserRoleService) { }

    ngOnInit(): void {
        this.search();
    }

    search(): void {
        this.userRoleService.searchUserRole(this.query)
        .subscribe(value => {
            console.log(value)
            this.listOfUserRole = of(value);
        }, err => {
            console.log(err);
        })
    }
}
