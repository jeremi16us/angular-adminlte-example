import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserRole } from 'src/app/model/user-role.model';
import { UserRoleService } from 'src/app/services/user-role.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-user-role-form',
    templateUrl: './user-role-form.component.html',
    styles: [
    ]
})
export class UserRoleFormComponent implements OnInit {

    public currentPageTitle = "User Role Form";
    public id = '';
    public userRole: UserRole;

    constructor(private activatedRoute: ActivatedRoute,
        private userRoleService: UserRoleService) { }

    ngOnInit(): void {
        this.activatedRoute.queryParams.subscribe(params => {
            this.id = params['id'];

            console.log("SELECTED ID : " + this.id);
            if (this.id) {
                this.getUserRole();
            } else {
                this.newUserRole();
            }
        });
    }

    newUserRole() {
      //  this.userRole = { _id: null }
    }

    getUserRole(): void {
        this.userRoleService.getUserRole(this.id).subscribe(value => {
            console.log(value);
            this.userRole = value;
        }, err => {
            console.log(err);
        });
    }

    saveUserRole(): void {
        this.userRoleService.saveUserRole(this.userRole).subscribe(value => {
            console.log(value);
        }, err => {
            console.log(err);
        });
    }
}
