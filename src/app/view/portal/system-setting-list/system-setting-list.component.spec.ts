import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemSettingListComponent } from './system-setting-list.component';

describe('SystemSettingListComponent', () => {
  let component: SystemSettingListComponent;
  let fixture: ComponentFixture<SystemSettingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemSettingListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemSettingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
