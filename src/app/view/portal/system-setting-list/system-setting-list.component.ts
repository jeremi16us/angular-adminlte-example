import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-system-setting-list',
    templateUrl: './system-setting-list.component.html',
    styles: [
    ]
})
export class SystemSettingListComponent implements OnInit {
    
    public currentPageTitle = "System Setting List";
    constructor() { }

    ngOnInit(): void {
    }

}
