import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-side-nav',
    templateUrl: './side-nav.component.html',
    styles: [
    ]
})
export class SideNavComponent implements OnInit {

    public appName = environment.app.name;
    public listOfMenu = [
        { 
            name: 'Dashboard', url: '/portal/dashboard',  
            icon: 'nav-icon fas fa-tachometer-alt',
            hasSubMenu: false, isNavHeader: false,
            listOfMenu: [
                { 
                    name: 'Dashboard', url: '/portal/dashboard',  
                    icon: 'far fa-circle nav-icon',
                    hasSubMenu: false, isNavHeader: false
                }
            ]
        }, /*{ 
            name: 'MISCELLANEOUS', url: '',  
            icon: '',
            hasSubMenu: false, isNavHeader: true,
            listOfMenu: [ ]
        }, { 
            name: 'Documentation', url: '',  
            icon: 'nav-icon fas fa-file',
            hasSubMenu: false, isNavHeader: false,
            listOfMenu: [ ]
        }, */{ 
            name: 'User Management', url: '',  
            icon: 'nav-icon fa fa-users',
            hasSubMenu: true, isNavHeader: false,
            listOfMenu: [
                { 
                    name: 'Users', url: '/portal/user-list',  
                    icon: 'fa fa-caret-right nav-icon',
                    hasSubMenu: false, isNavHeader: false
                },{ 
                    name: 'User Roles', url: '/portal/user-role-list',  
                    icon: 'fa fa-caret-right nav-icon',
                    hasSubMenu: false, isNavHeader: false
                }
            ]
        }, { 
            name: 'Setup', url: '',  
            icon: 'nav-icon fa fa-cogs',
            hasSubMenu: true, isNavHeader: false,
            listOfMenu: [
                { 
                    name: 'System Setting', url: '/portal/system-setting-list',  
                    icon: 'fa fa-caret-right nav-icon',
                    hasSubMenu: false, isNavHeader: false
                },{ 
                    name: 'Configuration', url: '/portal/configuration-list',  
                    icon: 'fa fa-caret-right nav-icon',
                    hasSubMenu: false, isNavHeader: false
                }
            ]
        }
    ];
    constructor() { }

    ngOnInit(): void {
    }

}
