import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/model/user.model';
import { UserService } from 'src/app/services/user.service';
import { of, Observable } from 'rxjs';

@Component({
    selector: 'app-user-list',
    templateUrl: './user-list.component.html',
    styles: [
    ]
})
export class UserListComponent implements OnInit {

    public currentPageTitle = "User List";
    public listOfUser: Observable<User[]>;
    public query = "";

    constructor(private userService: UserService) { }

    ngOnInit(): void {
        this.search();
    }

    search(): void {
        this.userService.searchUser(this.query)
        .subscribe(value => {
            console.log(value)
            this.listOfUser = of(value);
        }, err => {
            console.log(err);
        })
    }

}
