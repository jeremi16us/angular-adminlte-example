import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-configuration-list',
  templateUrl: './configuration-list.component.html',
  styles: [
  ]
})
export class ConfigurationListComponent implements OnInit {

    public currentPageTitle = "Configuration List";
  constructor() { }

  ngOnInit(): void {
  }

}
