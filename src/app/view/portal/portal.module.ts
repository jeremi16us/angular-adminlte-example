import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthService } from '../../services/auth.service';
import { TokenInterceptorService } from '../../services/token-interceptor.service';
import { AuthGuard } from '../../guard/auth.guard';

import { PortalRoutingModule, portalRoutingComponets } from './portal-routing.module';
import { PortalComponent } from './portal.component';
import { BreadcrumbComponent } from './common/breadcrumb/breadcrumb.component';
import {MatSnackBarModule} from '@angular/material/snack-bar'


@NgModule({
    declarations: [
        PortalComponent,
        portalRoutingComponets,
        BreadcrumbComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        HttpClientModule,
        MatSnackBarModule,
        ReactiveFormsModule,
        PortalRoutingModule,
    ],
    providers: [
        AuthService,
        AuthGuard,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: TokenInterceptorService,
          multi: true
        }
    ]
})
export class PortalModule { }
