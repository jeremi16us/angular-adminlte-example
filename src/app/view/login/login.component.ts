import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styles: [
    ]
})
export class LoginComponent implements OnInit {

    loginUserData = {"email":null, "password":null};
    constructor(private _authService: AuthService, private _router: Router) { }

    ngOnInit(): void {
    }

    loginUser(){
        console.log('Form Data: ' + JSON.stringify(this.loginUserData));
        this._authService.loginUser(this.loginUserData)
            .subscribe(
                res => {
                    console.log(res);
                    localStorage.setItem('token', res.token);
                    localStorage.setItem('currentUser', res.matchedUser);
                    this._router.navigate(['/portal']);
                },
                err => console.log(err)
            );
    }

}
