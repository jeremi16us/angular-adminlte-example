export interface User {
    _id: string;
    email: String;
    password: String;
    first_name?: String;
    middle_name?: String;
    last_name?: String;
}
