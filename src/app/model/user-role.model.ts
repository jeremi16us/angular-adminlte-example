export interface UserRole {
    _id: string;
    name: String;
    description: String;
}
